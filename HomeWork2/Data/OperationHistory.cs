﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork2.Data
{
    public class OperationHistory
    {
        public int Id { get; set; }

        public DateTime DateOfOperation { get; set; }

        public TypeOfOperation TypeOfOperation { get; set; }

        public double Amount { get; set; }
        public int AccountId { get; set; }
    }

    public enum TypeOfOperation
    {
        Debit, Credit
    }
}
