﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HomeWork2.Data
{
    class DataBase
    {
        private List<User> _users = new List<User>();
        private List<Account> _accounts = new List<Account>();
        private List<OperationHistory> _operationsHistory = new List<OperationHistory>();

        public void Initialize()
        {
            _users.Add(new User { Id = 1, FirstName = "user A", LastName = "A", Phone = "111-11-11", Passport = "Passport №1", 
                RegistrationDate = new DateTime(2020, 1, 1), Login = "user A", Password = "_a"});
            _users.Add(new User { Id = 2, FirstName = "user B", LastName = "Bb", Phone = "222-22-22", Passport = "Passport №2", 
                RegistrationDate = new DateTime(2020, 2, 1), Login = "user B", Password = "_b"});
            _users.Add(new User { Id = 3, FirstName = "user C", LastName = "cC", Phone = "333-33-33", Passport = "Passport №3", 
                RegistrationDate = new DateTime(2020, 3, 1), Login = "user C", Password = "_c"});
            _users.Add(new User { Id = 4, FirstName = "user D", LastName = "DD", Phone = "444-44-44", Passport = "Passport №4", 
                RegistrationDate = new DateTime(2020, 4, 1), Login = "user D", Password = "_d"});
            _users.Add(new User { Id = 5, FirstName = "user E", LastName = "ee", Phone = "555-55-55", Passport = "Passport №5", 
                RegistrationDate = new DateTime(2020, 5, 1), Login = "user E", Password = "_e"});

            _accounts.Add(new Account { Id = 1, OpeningDate = new DateTime(2020, 1, 1), Amount = 10000.00, UserId = 1 });
            _accounts.Add(new Account { Id = 2, OpeningDate = new DateTime(2020, 1, 2), Amount = 2000.00, UserId = 1 });
            _accounts.Add(new Account { Id = 3, OpeningDate = new DateTime(2020, 2, 2), Amount = 300.00, UserId = 2 });
            _accounts.Add(new Account { Id = 4, OpeningDate = new DateTime(2020, 3, 1), Amount = 40.00, UserId = 3 });
            _accounts.Add(new Account { Id = 5, OpeningDate = new DateTime(2020, 3, 3), Amount = 500.00, UserId = 3 });
            _accounts.Add(new Account { Id = 6, OpeningDate = new DateTime(2020, 4, 4), Amount = 6000.00, UserId = 4 });
            _accounts.Add(new Account { Id = 7, OpeningDate = new DateTime(2020, 5, 7), Amount = 70000.00, UserId = 5 });

            // account 1
            _operationsHistory.Add(new OperationHistory { Id = 1, DateOfOperation = new DateTime(2020, 1, 2),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 4400.00, AccountId = 1});
            _operationsHistory.Add(new OperationHistory { Id = 2, DateOfOperation = new DateTime(2020, 1, 2),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 800.00, AccountId = 1});
            _operationsHistory.Add(new OperationHistory { Id = 3, DateOfOperation = new DateTime(2020, 1, 4),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 1100.00, AccountId = 1});

            // account 2
            _operationsHistory.Add(new OperationHistory { Id = 4, DateOfOperation = new DateTime(2020, 1, 2),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 90.00, AccountId = 2});
            _operationsHistory.Add(new OperationHistory { Id = 5, DateOfOperation = new DateTime(2020, 1, 3),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 560.00, AccountId = 2});

            // account 3
            _operationsHistory.Add(new OperationHistory { Id = 6, DateOfOperation = new DateTime(2020, 2, 4),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 20.00, AccountId = 3});

            // account 5
            _operationsHistory.Add(new OperationHistory { Id = 7, DateOfOperation = new DateTime(2020, 3, 5),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 300.00, AccountId = 5});
            _operationsHistory.Add(new OperationHistory { Id = 8, DateOfOperation = new DateTime(2020, 3, 7),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 200.00, AccountId = 5});
            _operationsHistory.Add(new OperationHistory { Id = 9, DateOfOperation = new DateTime(2020, 3, 7),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 100.00, AccountId = 5});

            // account 6
            _operationsHistory.Add(new OperationHistory { Id = 10, DateOfOperation = new DateTime(2020, 4, 6),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 500.00, AccountId = 6});
            _operationsHistory.Add(new OperationHistory { Id = 11, DateOfOperation = new DateTime(2020, 4, 8),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 3000.00, AccountId = 6});
            _operationsHistory.Add(new OperationHistory { Id = 12, DateOfOperation = new DateTime(2020, 5, 10),
                TypeOfOperation = TypeOfOperation.Debit, Amount = 500.00, AccountId = 6});
            _operationsHistory.Add(new OperationHistory { Id = 13, DateOfOperation = new DateTime(2020, 5, 11),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 3500.00, AccountId = 6});

            // account 7
            _operationsHistory.Add(new OperationHistory { Id = 14, DateOfOperation = new DateTime(2020, 5, 7),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 1500.00, AccountId = 7});
            _operationsHistory.Add(new OperationHistory { Id = 15, DateOfOperation = new DateTime(2020, 5, 7),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 28000.00, AccountId = 7});
            _operationsHistory.Add(new OperationHistory { Id = 16, DateOfOperation = new DateTime(2020, 5, 7),
                TypeOfOperation = TypeOfOperation.Credit, Amount = 340.00, AccountId = 7});            
        }

        public void GetUserInformation(string login, string password)
        {
            var user = _users.FirstOrDefault(x => x.Login == login && x.Password == password);
            if (user == null)
                Console.WriteLine($"Не найден аккаунт соответствующий логину '{login}' и паролю '{password}'");
            else
                Console.WriteLine($"Данные пользователя:" + Environment.NewLine
                    + $"Имя: {user.FirstName}, фамилия: {user.LastName}" + Environment.NewLine
                    + $"Дата регистрации: {user.RegistrationDate.ToString("d")}" + Environment.NewLine
                    + $"Пасспорт: {user.Passport}, телефон: {user.Phone}");
            Console.WriteLine();
        }

        public void GetAccountsByUser(int userId)
        {
            var accounts = _accounts.Where(x => x.UserId == userId);

            if (accounts.Count() == 0)
                Console.WriteLine("У пользователя отсутствуют счета");
            else
            {
                CultureInfo culture = CultureInfo.CurrentCulture;
                Console.WriteLine($"Счета пользователя с ID {userId}:");
                Console.OutputEncoding = Encoding.Unicode; // иначе вместо символа '₽' выводится '?'

                foreach (Account account in accounts)
                    Console.WriteLine($"ID: {account.Id}, дата открытия: {account.OpeningDate.ToString("d")}, " +
                        $"баланс: {account.Amount.ToString("C2", culture)}" );
            }
            Console.WriteLine();
        }

        public void GetAccountsAndOperationsHistoryByUser(int userId)
        {
            var accounts = _accounts.Where(x => x.UserId == userId).OrderBy(x => x.Id);

            if (accounts.Count() == 0)
            {
                Console.WriteLine("У пользователя отсутствуют счета \n");
                return;
            }
            
            var operations = accounts.Join(_operationsHistory,
                                       account => account.Id,
                                       operation => operation.AccountId,
                                       (account, operation) => (account, operation)).ToArray();

            CultureInfo culture = CultureInfo.CurrentCulture;
            Console.OutputEncoding = Encoding.Unicode; // иначе вместо символа '₽' выводится '?'

            foreach (Account account in accounts)
            {
                Console.WriteLine($"ID счёта: {account.Id}, дата открытия: {account.OpeningDate.ToString("d")}, " +
                    $"баланс: {account.Amount.ToString("C2", culture)}");
                var currentOperations = operations.Where(x => x.account == account).OrderBy(x => x.operation.DateOfOperation);

                if (currentOperations.Count() == 0)
                    Console.WriteLine("Отсутствуют операции по счёту \n");
                else
                {
                    Console.WriteLine("Операции по счёту:");
                    foreach (var operation in currentOperations)
                        Console.WriteLine($"ID: {operation.operation.Id}, " +
                            $"дата операции: {operation.operation.DateOfOperation.ToString("d")}, " +
                            $"тип операции: {operation.operation.TypeOfOperation}, " +
                            $"сумма операции: {operation.operation.Amount.ToString("C2", culture)}");
                    Console.WriteLine();
                }
            }
        }

        public void GetAllDebitOperationsFromHistory()
        {
            var debitOperations = _operationsHistory.Where(x => x.TypeOfOperation == TypeOfOperation.Debit);

            if (debitOperations.Count() == 0)
            {
                Console.WriteLine("Отсутствуют операции пополнения счетов");
                return;
            }

            var result = from operation in debitOperations
                         join account in _accounts
                             on operation.AccountId equals account.Id
                         select new { Account = account, Operation = operation } into temp
                         join user in _users
                             on temp.Account.UserId equals user.Id
                         select new { User = user, Account = temp.Account, Operation = temp.Operation };

            CultureInfo culture = CultureInfo.CurrentCulture;
            Console.OutputEncoding = Encoding.Unicode; // иначе вместо символа '₽' выводится '?'

            foreach (var user in result.GroupBy(x => x.User).OrderBy(x => x.Key.Id))
            {
                Console.WriteLine($"ID пользователя: {user.Key.Id }");
                foreach (var account in user.GroupBy(x => x.Account).OrderBy(x => x.Key.Id))
                {
                    Console.WriteLine($"Дебетовые операции по счёту с ID: {account.Key.Id}");
                    foreach (var operation in account.GroupBy(x => x.Operation).OrderBy(x => x.Key.Id))
                        Console.WriteLine($"ID: {operation.Key.Id}, " +
                                          $"дата операции: {operation.Key.DateOfOperation.ToString("d")}, " +
                                          $"сумма операции: {operation.Key.Amount.ToString("C2", culture)}");
                }
                Console.WriteLine();
            }            
        }

        public void GetCurrentAccountBalanceByLimit(double limit)
        {
            var result = (from account in _accounts
                          join user in _users
                              on account.UserId equals user.Id
                          select new { User = user,
                              Amount = account.Amount })
                         .Union(from operation in _operationsHistory
                                join account in _accounts
                                    on operation.AccountId equals account.Id
                                join user in _users
                                    on account.UserId equals user.Id
                                select new { User = user,
                                    Amount = (operation.TypeOfOperation == TypeOfOperation.Debit ? operation.Amount : -operation.Amount)})
                         .GroupBy(x => new { x.User })
                         .Select(x => new { User = x.Key.User, Amount = x.Sum(y => y.Amount) })
                         .Where(x => x.Amount > limit);
            
            CultureInfo culture = CultureInfo.CurrentCulture;
            Console.OutputEncoding = Encoding.Unicode; // иначе вместо символа '₽' выводится '?'

            foreach (var item in result.OrderBy(x => x.User.Id))
            {
                Console.WriteLine($"user: {item.User.Id}, amount: {item.Amount}");
            }
            Console.WriteLine();
        }
    }    
}
