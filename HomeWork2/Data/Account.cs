﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork2.Data
{
    public class Account
    {
        public int Id { get; set; }

        public DateTime OpeningDate { get; set; }

        public double Amount { get; set; }

        public int UserId { get; set; }
    }
}
