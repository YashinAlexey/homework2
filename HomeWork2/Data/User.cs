﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork2.Data
{
    class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
                
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Passport { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}
