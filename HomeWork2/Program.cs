﻿using HomeWork2.Data;
using System;

namespace HomeWork2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            db.Initialize();

            Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю \n");
            db.GetUserInformation("user A", "_a"); // Существующий пользователь
            db.GetUserInformation("user A", "b"); // Несуществующий пользователь

            Console.WriteLine("2.Вывод данных о всех счетах заданного пользователя \n");
            db.GetAccountsByUser(1); // Существующий пользователь
            db.GetAccountsByUser(7); // Несуществующий пользователь

            Console.WriteLine("3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту \n");
            db.GetAccountsAndOperationsHistoryByUser(3); // Существующий пользователь
            db.GetAccountsAndOperationsHistoryByUser(7); // Несуществующий пользователь

            Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта \n");
            db.GetAllDebitOperationsFromHistory();

            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой) \n");
            db.GetCurrentAccountBalanceByLimit(300);

            Console.ReadLine();
        }
    }
}
